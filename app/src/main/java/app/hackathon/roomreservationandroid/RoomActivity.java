package app.hackathon.roomreservationandroid;

import android.database.Cursor;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import static app.hackathon.roomreservationandroid.Contract.COLUMN_NAME_NAME;
import static app.hackathon.roomreservationandroid.Contract.COLUMN_NAME_STATUS;

public class RoomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_view);
        String rooName = getIntent().getStringExtra("roomName");
        ConstraintLayout layout = (ConstraintLayout) findViewById(R.id.roomViewLayout);
        DatabaseHelper databaseHelper = new DatabaseHelper(RoomActivity.this);
        Cursor roomFromDb = databaseHelper.getByRoomName(rooName);

        TextView roomNameView = (TextView)layout.findViewById(R.id.textViewRoomName);
        TextView statusValueView = (TextView) layout.findViewById(R.id.textViewStatus);
        //cos sie jebie z cursorem trzeba sprawdzic i ustawic reszte pol tak jak status i name ponizej
        roomNameView.setText(roomFromDb.getString(roomFromDb.getColumnIndexOrThrow(COLUMN_NAME_NAME)));
        statusValueView.setText(roomFromDb.getString(roomFromDb.getColumnIndexOrThrow(COLUMN_NAME_STATUS)));
    }

}