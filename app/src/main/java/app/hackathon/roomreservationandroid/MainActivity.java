package app.hackathon.roomreservationandroid;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import static app.hackathon.roomreservationandroid.Contract.COLUMN_NAME_BUILDING_NAME;
import static app.hackathon.roomreservationandroid.Contract.COLUMN_NAME_NAME;
import static app.hackathon.roomreservationandroid.Contract.COLUMN_NAME_OWNER;
import static app.hackathon.roomreservationandroid.Contract.COLUMN_NAME_STATUS;

public class MainActivity extends AppCompatActivity {

    GridLayout mainGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        recreateBasicData(databaseHelper, "gryffindor", "Stern", "jk82fu");
        recreateBasicData(databaseHelper, "slytherin", "Stern", "jk82fu");

        setContentView(R.layout.activity_main);

        mainGrid = (GridLayout) findViewById(R.id.mainGrid);

        setSingleEvent(mainGrid);
    }

    private void setSingleEvent(GridLayout mainGrid) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            final CardView cardView = (CardView) mainGrid.getChildAt(i);

            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardView currentCardView = (CardView) view;
                    LinearLayout layoutUnderCurrentCardView = (LinearLayout)currentCardView.getChildAt(0);
                    TextView textViewUnderCurrentLayout = (TextView) layoutUnderCurrentCardView.getChildAt(0);
                    Intent intent = new Intent(MainActivity.this, ActivityOne.class);
                    intent.putExtra("buildingName", textViewUnderCurrentLayout.getText());

                    startActivity(intent);

                }
            });
        }
    }

    private void recreateBasicData(DatabaseHelper databaseHelper, String roomName, String buildingName, String ownerName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME_NAME, roomName);
        contentValues.put(COLUMN_NAME_OWNER, ownerName);
        contentValues.put(COLUMN_NAME_BUILDING_NAME, buildingName);
        contentValues.put(COLUMN_NAME_STATUS, "free");
        databaseHelper.addData(contentValues);
    }
}
