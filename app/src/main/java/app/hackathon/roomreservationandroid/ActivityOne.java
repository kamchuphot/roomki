package app.hackathon.roomreservationandroid;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityOne extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        Cursor data = databaseHelper.getRoomByBuildingName(getIntent().getStringExtra("buildingName"));
        CustomCursorAdapter customCursorAdapter = new CustomCursorAdapter(this, data);
        setListAdapter(customCursorAdapter);

        ListView listView = getListView();
        listView.setTextFilterEnabled(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Intent intent = new Intent(ActivityOne.this, RoomActivity.class);
                TextView textView = (TextView) view.findViewById(R.id.roomItem);
                intent.putExtra("roomName", textView.getText());

                startActivity(intent);

            }
        });
    }
}
