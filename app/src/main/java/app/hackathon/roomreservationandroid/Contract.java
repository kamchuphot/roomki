package app.hackathon.roomreservationandroid;

import android.provider.BaseColumns;

public final class Contract implements BaseColumns{
    private Contract(){}

    static final String TABLE_NAME = "room";
    static final String COLUMN_NAME_NAME = "name";
    static final String COLUMN_NAME_STATUS = "isTaken";
    static final String COLUMN_NAME_NEAREST_FREE_DATE = "freeDate";
    static final String COLUMN_NAME_OWNER = "owner";
    public static final String COLUMN_NAME_BUILDING_NAME = "buildingName";
}
