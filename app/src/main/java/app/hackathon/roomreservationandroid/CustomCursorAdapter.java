package app.hackathon.roomreservationandroid;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomCursorAdapter extends CursorAdapter {

    CustomCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.activity_my_list, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView listView = view.findViewById(R.id.roomItem);
        String body = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        listView.setText(body);
        ImageView imageView = view.findViewById(R.id.roomLogo);
        imageView.setImageResource(getSource(body, context));

    }

    private int getSource(String src, Context context) {
        return context.getResources().getIdentifier(src, "drawable", context.getPackageName());
    }
}
