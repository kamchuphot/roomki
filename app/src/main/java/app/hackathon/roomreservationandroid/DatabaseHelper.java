package app.hackathon.roomreservationandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static app.hackathon.roomreservationandroid.Contract.COLUMN_NAME_BUILDING_NAME;
import static app.hackathon.roomreservationandroid.Contract.COLUMN_NAME_NAME;
import static app.hackathon.roomreservationandroid.Contract.TABLE_NAME;

public final class DatabaseHelper extends SQLiteOpenHelper {
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    Contract._ID + " INTEGER PRIMARY KEY," +
                    COLUMN_NAME_NAME + " TEXT," +
                    Contract.COLUMN_NAME_BUILDING_NAME + " TEXT," +
                    Contract.COLUMN_NAME_OWNER + " TEXT," +
                    Contract.COLUMN_NAME_STATUS + " BOOLEAN," +
                    "CONSTRAINT unique_name UNIQUE (" + COLUMN_NAME_NAME + "));";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;


    public DatabaseHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void clearData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public boolean addData(ContentValues contentValues){
        SQLiteDatabase writableDatabase = this.getWritableDatabase();

        long result = writableDatabase.insert(TABLE_NAME, null, contentValues);

        //if date as inserted incorrectly it will return -1
        return result != -1;
    }

    /**
     * Returns all the data from database
     * @return
     */
    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    /**
     * Returns room by it's name
     * @param name
     * @return
     */
    public Cursor getByRoomName(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_NAME_NAME + " = '" + name + "'";
        return db.rawQuery(query, null);
    }

    public Cursor getRoomByBuildingName(String buildingName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_NAME_BUILDING_NAME + " = '" + buildingName + "'";
        return db.rawQuery(query,null);
    }
}